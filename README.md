# ContactMe

**ContactMe** is a Telegram bot wrote with the [python-telegram-bot library](https://github.com/python-telegram-bot/python-telegram-bot) v12, and licensed under the [GNU Affero GPL](https://www.gnu.org/licenses/agpl-3.0.html). 
This bot can be used to be contacted by other users and chat with them.

It is a useful tool *if you are a user*: this way you can be contacted by those users who are limited _(those who cannot message non-contacts on Telegram)_, will be contacted in only one chat and do not have to reveal your real account. 

And it is a useful tool *if you have a channel* or another kind of project on Telegram: you will have a professional and simple bot from user will be able to contact you, that can be used by all your staff to answer to users. 

### How does it works?

When a user contacts the bot, his messages are forwarded to all the staff. 
Then, the staff can answer to this message. The staff's answer are sent to the user in such a way the staff's member identity has not to be revealed. Others staff members are notified too, so that all of them can follow the conversation. 

### Main functions

- Compatibility with all kind of messages; 
- Simple chat with users; 
- Possibility to ban and unban users;
- Designed to be used by an entire staff;
- Multithread.

### Credits

This project is currently maintained by leonedavide[at]protonmail.com.  

<div><a href=https://www.flaticon.com/free-icon/chat_236850#term=chat&page=1&position=91>Icon</a> made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/"title="Flaticon">www.flaticon.com</a></div>
