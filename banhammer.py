# -*- coding: utf-8 -*-

""" Distributed under GNU Affero GPL license.

    ContactMe v2.0 28/08/2019.

    Copyright (C) 2019  Davide Leone <leonedavide[at]protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#--- LIBRARIES IMPORT ---
from config import banned_users_file
from functools import wraps
from pathlib import Path

#--- NEEDED FILES CHECK ---
id_list = Path(banned_users_file)
if not id_list.is_file():
    #The banned_users_file was not created yet
    open(banned_users_file,'w')

#--- FUNCTIONS ---

def get_banned_users():
    #This function reads the .txt file with all the ids and returns them
    
    with open(banned_users_file,'r') as file:
        banned_users = file.read()

    banned_users = banned_users.replace('\n\n','\n') #Avoid double empty line

    return banned_users

def get_banned_users_as_list():
    #Gets a list type with all the banned users' ids
    #The list is designed to contain only ints, to better fit Telegram id type
    banned_users = get_banned_users()
    banned_users = banned_users.split()
    banned_users = [int(user_id) for user_id in banned_users]
    return banned_users

def ban_user(user_id):
    #Used to ban a user; return true is success, false if not
    user_id = str(user_id)
    banned_users = get_banned_users()

    if (not is_banned(user_id)):
        if banned_users[-1:] != "\n":
            #The last line is not an empty line; add an empty line
            banned_users += "\n"
            
        banned_users += user_id+"\n"
        
        with open(banned_users_file,'w') as file:
            file.write(banned_users)
            
        return True

    return False

def block_banned(func):
    #A decorator to obstruct access to a handler for banned users
    @wraps(func)
    
    def wrapped(update, context, *args, **kwargs):
        user_id = update.effective_user.id
        
        if user_id in get_banned_users_as_list(): 
            return
        
        return func(update, context, *args, **kwargs)
    
    return wrapped


def unban_user(user_id):
    #Used to unban a user; return true is success, false if not
    user_id = str(user_id)
    banned_users = get_banned_users()

    if is_banned(user_id):
        banned_users = banned_users.replace(user_id,'')
        
        with open(banned_users_file,'w') as file:
            file.write(banned_users)
            
        return True

    return False

def is_banned(user_id):
    #Returns true if the user is banned, false if it's not
    user_id = str(user_id)
    banned_users = get_banned_users()
    return (user_id in banned_users)
