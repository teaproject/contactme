# -*- coding: utf-8 -*-

""" Distributed under GNU Affero GPL license.

    ContactMe v2.0 28/08/2019.

    Copyright (C) 2019  Davide Leone <leonedavide[at]protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from config import *
import banhammer
from banhammer import block_banned
import echo
import staff_manager
from staff_manager import staff_only_function, staff_excluded_function

#--- USER'S FUNCTIONS ---
#Before every function various decorators (like "@run_async") are placed
#@run_async allow that function to work in multithread
#@block_banned, @staff_excluded_function and @staff_only_function are used as filters to allow only some users to use that function
#telegram.ext.Filters are not used because they are not dynamic - if a user is added to the banned list, a telegram.ext.Filters' filter would not block him until a software reset

@block_banned #Banned users are not allowed to use this function
@run_async #Multithreading
@staff_only_function #Only admins are allowed to use this function
def admin_message_callback(update,context):
    #When an admin replies to someone, sends the admin's message to him
    text_error = "I could not sent the message!"
    reply_to_id = staff_manager.admin_answer_to(update)

    if reply_to_id == False:
        success = False
        text_error = "The user has disabled the link to his account for forwarded messages, I can't identify him!"

    else:
        try:
            success = echo.send_anything(reply_to_id, update.message) 
        except telegram.error.Unauthorized:
            success = False
            text_error = "I could not sent the message, as the user has blocked the bot!"

    if success:
        update.message.reply_text(text = "Message sent successfully!")
        admin_inline_mention = '<a href="tg://user?id={}">{}</a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))

        try:
            user_name = bot.get_chat(reply_to_id).first_name
        except telegram.error.Unauthorized:
            user_name = "this user"
        
        user_inline_mention = '<a href="tg://user?id={}">{}</a>'.format(reply_to_id, html.escape(user_name))        
        text = "\U00002b06 The message above was sent by {admin} to {user}.\nAnswer <b>this</b> message is equal to answer to {admin}".format(admin = admin_inline_mention, user= user_inline_mention)
        staff_manager.forward_message(update, update.effective_user.id) 
        staff_manager.send_message(text, update.effective_user.id)
        
    else:
        update.message.reply_text(text = text_error)

@block_banned #Banned users are not allowed to use this function
@staff_only_function #Only admins are allowed to use this function
def ban_callback(update,context):    
    #/ban command
    user_id = staff_manager.admin_answer_to(update)

    if user_id == False:
        ban_success = False
    else:
        ban_success = banhammer.ban_user(user_id)

    if ban_success:
        update.message.reply_text(text = "User banned with success.")
        admin_inline_mention = '<a href="tg://user?id={}">{} </a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))

        try:
            user_name = bot.get_chat(user_id).first_name
        except telegram.error.Unauthorized:
            user_name = "this user"
        
        user_inline_mention = '<a href="tg://user?id={}">{} </a>'.format(user_id, html.escape(user_name))
        text = "{admin} has banned {user} from the use of the bot.\nAnswer to <b>this</b> message will be equal to answer to {admin}".format(admin = admin_inline_mention,user = user_inline_mention)
        #The staff is informed of the ban
        #Please notice that the admin inline_mention has to be the first as the first mention is used to extract the ID 
        staff_manager.send_message(text,update.message.chat_id)

        try:
            bot.send_message(
                chat_id = user_id,
                text = "I'm sorry, you have been banned \U0001F614"
            )    
        except telegram.error.Unauthorized:
            None
    else:
        update.message.reply_text(text = "I could not ban this user, as he was already banned.")
        
@run_async #Multithreading
@block_banned #Banned users are not allowed to use this function
def license_callback(update, context):
    #/license command
    user_id = update.message.chat_id
    message = "This software is distributed under the [GNU Affero GPL](https://www.gnu.org/licenses/agpl.html) license."

    update.message.reply_markdown(
        text = message,
        reply_markup = InlineKeyboardMarkup(
            [[
        InlineKeyboardButton(
            text = 'Source code',
            url = source_url
                )
             ]]
            ),
        disable_web_preview = True
        )

def mod_callback(update, context):
    #/mod command
    user_id = staff_manager.admin_answer_to(update)

    if user_id == False:
        success = False
    else:
        success = staff_manager.add_user_to_staff(user_id)

    if success:
        update.message.reply_text(
            text = "User added to the staff with success!"
            )
    else:
        update.message.reply_text(
            text = "I could not add the user to the staff"
            )

@block_banned #Banned users are not allowed to use this function
@run_async #Multithreading
def not_understand_callback(update, context):
    #Warns the user that his message could not be processed
    update.message.reply_text(text = "I'm sorry, I did not understand")

@run_async #Multithreading
@block_banned #Banned users are not allowed to use this function
def start_callback(update, context):
    #/start, /help, /info commands
    update.message.reply_markdown(text = "Welcome \U0001F60A.\n\nThrough this bot you can communicate with @DavideLeone. Just send me everything and I will do the hard work for you.\n\nUse /license to discover more about the bot's license.")

@block_banned #Banned users are not allowed to use this function
@staff_only_function #Only admins are allowed to use this function
def unban_callback(update,context):
    #/unban command
    user_id = staff_manager.admin_answer_to(update)

    if user_id == False:
        unban_success = False
    else:
        unban_success = banhammer.unban_user(user_id)

    if unban_success:
        update.message.reply_text(text = "User unbanned with success.")
        admin_inline_mention = '<a href="tg://user?id={}">{} </a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))

        try:
            user_name = bot.get_chat(user_id).first_name
        except telegram.error.Unauthorized:
            user_name = "this user"
        
        user_inline_mention = '<a href="tg://user?id={}">{} </a>'.format(user_id, html.escape(user_name))
        text = "{admin} has unbanned {user} from the use of the bot.\nAnswer to <b>this</b> message will be equal to answer to {admin}".format(admin = admin_inline_mention,user = user_inline_mention)
        #The staff is informed of the ban
        #Please notice that the admin inline_mention has to be the first as the first mention is used to extract the ID 
        staff_manager.send_message(text,update.message.chat_id)

        try:
            bot.send_message(
                chat_id = user_id,
                text = "Happy news! You have been unbanned! \U0001F60A"
                )
        except telegram.error.Unauthorized:
            None
            
    else:
        update.message.reply_text(text = "I could not unban the user, as he was not banned.")

def unmod_callback(update, context):
    #/unmod command
    user_id = staff_manager.admin_answer_to(update)

    if user_id == False:
        success = False
    else:
        success = staff_manager.remove_user_from_staff(user_id)

    if success:
        update.message.reply_text(text = "User removed from the staff with success!")
    else:
        update.message.reply_text(text = "I could not remove the user from to the staff")


@block_banned #Banned users are not allowed to use this function
@run_async #Multithreading
@staff_excluded_function #Admins are not allowed to use this function
def user_message_callback(update, context):
    #The user messages (files and non-command text) are forwarded to the admin
    user_id = update.message.chat_id
    forward_message = staff_manager.forward_message(update)
    update.message.reply_text(text = "Message sent \U0001F60A!")

    if (forward_message.forward_from == None):
        #If the user by setting sends messages that do not link back to him when forwarded, they can't be used to answer to him
        #The bot place the user's id in a message sent to the admin, allowing him to answer to user
        inline_mention = '<a href="tg://user?id={}">{}</a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))
        staff_manager.send_message(text = "\U00002b06 This message was sent by {} \U00002b06\n\nIf you want to talk to him, please answer to <b>this</b> message, not the forwarded one!".format(inline_mention))            

    elif (update.message.sticker != None) or (update.message.audio != None) or (update.message.animation != None) or (update.message.game != None):
        #All this message types does not, in the official mobile client, tell you who sent the messsage (the forward label is hidden)
        #The user is informed by the bot about who sent the message
        inline_mention = '<a href="tg://user?id={}">{}</a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))
        staff_manager.send_message(text = "\U00002b06 This message was sent by {} \U00002b06".format(inline_mention))

    elif (update.message.forward_from != None):
        #If you are talking with the user Alice, and she forward to the bot a message from Bob, you got a message forwarded from Bob
        #The bot tells you that that message is from Alice, so you know who are you talking to
        #Since when you answer to a message the bot check from who that message was forwarded and send your message to him
        #If you answer to the message sent by Alice but which appear as "forwarded from Bob" the bot will contact Bob, not Alice
        #To avoid this the inline mention that tells you the message is from Alice is used
        #There is placed the Alice id as an entity, so if you answer to the bot's message you will contact Alice
        inline_mention = '<a href="tg://user?id={}">{} </a>'.format(update.effective_user.id, html.escape(update.effective_user.first_name))
        staff_manager.send_message(text = "\U00002b06 This message was sent by {} \U00002b06\n\nIf you want to talk to him, please answer to <b>this</b> message, not the forwarded one!".format(inline_mention))

# --- HANDLING AND POLLING ---
admin_message_handler = MessageHandler(
    callback = admin_message_callback,
    filters = (Filters.private & Filters.reply)
    )

ban_handler = CommandHandler(
    command = 'ban',
    callback = ban_callback,
    filters = (Filters.private & Filters.reply)
    )

license_handler = CommandHandler(
    command = 'license',
    callback = license_callback,
    filters = (Filters.private)
    )

mod_handler = CommandHandler(
    command = 'mod',
    callback = mod_callback,
    filters = (Filters.private & Filters.user(owner_id))
    )

not_understand_handler = MessageHandler(
    callback = not_understand_callback,
    filters = (Filters.all)
    )

start_handler = CommandHandler(
    command = ['start','help','info'],
    callback = start_callback,
    filters = (Filters.private)
    )


unban_handler = CommandHandler(
    command = 'unban',
    callback = unban_callback,
    filters = (Filters.private & Filters.reply)
    )

unmod_handler = CommandHandler(
    command = 'unmod',
    callback = unmod_callback,
    filters = (Filters.private & Filters.user(owner_id))
    )

user_message_handler = MessageHandler(
    callback = user_message_callback,
    filters = (Filters.private)
    )

handlers = [start_handler, license_handler, mod_handler, unmod_handler, ban_handler, unban_handler,
            admin_message_handler, user_message_handler, not_understand_handler]


for handler in handlers:
    dispatcher.add_handler(handler)
    
updater.start_polling()
updater.idle() 