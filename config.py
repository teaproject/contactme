# -*- coding: utf-8 -*-

""" Distributed under GNU Affero GPL license.

    ContactMe v2.0 28/08/2019.

    Copyright (C) 2019  Davide Leone <leonedavide[at]protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#--- LIBRARIES IMPORT ---
import html
import logging
from pathlib import Path
import telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, BaseFilter
from telegram.ext.dispatcher import run_async
import time

#--- VARIABLES DEFINITION ---
api_token = ''
owner_id = 0
banned_users_file = 'banned_users.txt'
staff_users_file = 'staff_users.txt'
source_url = 'https://gitlab.com/tea-project/contactme'

#--- STRICTLY-BOT-RELATED VARIABLES ---
updater = Updater(token=api_token, use_context=True)
dispatcher = updater.dispatcher
bot = updater.dispatcher.bot
telegram.ext.MessageQueue() #Prevents the bot to trigger the Telegram's spam limit. Autostart by default

#--- LOGGING CONFIGURATION ---
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)